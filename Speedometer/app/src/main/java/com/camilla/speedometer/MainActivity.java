package com.camilla.speedometer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

//Code for location based on the one from http://developer.android.com/guide/topics/location/strategies.html

public class MainActivity extends Activity implements View.OnClickListener {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private int gMeasurement = 0;

    TextView speed;
    RadioButton metre;
    RadioButton kilometre;
    RadioButton knot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        speed = (TextView) findViewById(R.id.speed);
        metre = (RadioButton) findViewById(R.id.metre);
        kilometre = (RadioButton) findViewById(R.id.kilometre);
        knot = (RadioButton) findViewById(R.id.knot);

        metre.setOnClickListener(this);
        kilometre.setOnClickListener(this);
        knot.setOnClickListener(this);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean provider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Retrieves preferences
        SharedPreferences mySettings = getPreferences(MODE_PRIVATE);
        if(mySettings.contains(getString(R.string.measurement))) {
            gMeasurement = mySettings.getInt(getString(R.string.measurement), 0);
        }

        // Sets the appropriate button status to checked
        switch(gMeasurement) {
            case 0: metre.setChecked(true); break;
            case 1: kilometre.setChecked(true); break;
            case 2: knot.setChecked(true); break;
        }

        // Checks if the GPS provider is available on the device
        if(!provider) {
            Toast.makeText(this.getApplicationContext(), getString(R.string.GPS_error), Toast.LENGTH_LONG).show();
        }

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double theSpeed = location.getSpeed();
                String currentSpeed = new DecimalFormat("#.##").format(convertSpeed(theSpeed));
                speed.setText(currentSpeed);

                switch(gMeasurement) {
                    case 0: speed.append(" " + getText(R.string.m_per_s)); break;
                    case 1: speed.append(" " + getText(R.string.k_per_h)); break;
                    case 2: speed.append(" " + getText(R.string.kn_per_h)); break;
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.metre: gMeasurement = 0; break;
            case R.id.kilometre: gMeasurement = 1; break;
            case R.id.knot: gMeasurement = 2; break;
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Saves preferences (gMeasurement)
        SharedPreferences mySettings = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = mySettings.edit();
        editor.putInt(getString(R.string.measurement), gMeasurement);
        editor.apply();

        // Removes listener to save battery
        locationManager.removeUpdates(locationListener);
    }


    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    public double convertSpeed(double rawSpeed) {
        switch(gMeasurement) {
            // 0 = m/s. Raw speed is already in m/s, so just return it.
            case 0: Double conversionRate0 = Double.parseDouble(getString(R.string.m_per_s_conversion_rate));
                    return (rawSpeed * conversionRate0);

            // 1 = mp/h (English, default) or km/h (Norwegian).
            case 1: Double conversionRate1 = Double.parseDouble(getString(R.string.k_per_h_conversion_rate));
                    return (rawSpeed * conversionRate1);

            // 2 = knots.
            case 2: Double conversionRate2 = Double.parseDouble(getString(R.string.kn_per_h_conversion_rate));
                    return (rawSpeed * conversionRate2);
        }
        return 0;
    }
}
